# From Imports
from addict import Dict as D
from collections import OrderedDict as odd, namedtuple
from typing import Any, Union, Dict, MutableSequence as MS, List, Tuple

class _short_funcs:

	def length(self, range: bool = False):
		if len(self._keys) == len(self._values):
			return len(self._keys) - 1 if range else len(self._keys)

	def key(self, value: Any) -> Any:
		return self._keys[self._values.index(value)]

	def key_from_index(self, index: int) -> Any:
		return self._keys[index]

	def keys_from_slice(self, keys: slice) -> List[Any]:
		return self._keys[keys]

	def value_from_index(self, index: int) -> Any:
		return self._values[index]

	def replace(self, original: Any, replacement: Any):
		"""
			Find and Replace Value
		"""
		self._values[self._values.index(original)] = replacement

	# DONE
	def insert(self, key: Any, value: Any):
		"""
			This takes either a dict / tea object and inserts it after the given key, or takes a value and inserts it, and uses the average between the previous integer and next integer it finds as the associated key.
		"""
		index: int = self._keys.index(key)
		if isinstance(value, (dict, self.__class__)) and len(value) == 1:
			self._keys.insert(index, list(value.keys())[0])
			self._values.insert(index, list(value.values())[0])
		else:
			average_key: int = (
				(max([i for i in self._keys[:index] if isinstance(i, int)]) +
				min([i for i in self._keys[index:] if isinstance(i, int)])) /
				2
			)
			self._keys.insert(index, average_key)
			self._values.insert(index, value)

	# DONE
	def insert_by_index(self, index: int, value: Any):
		"""
			This takes either a dict / tea object and inserts it after the given index, or takes a value and inserts it, and uses the average between the previous integer and next integer it finds as the associated key.
		"""
		if isinstance(value, (dict, self.__class__)) and len(value) == 1:
			self._keys.insert(index, list(value.keys())[0])
			self._values.insert(index, list(value.values())[0])
		else:
			average_key: int = (
				(max([i for i in self._keys[:index] if isinstance(i, int)]) +
				min([i for i in self._keys[index:] if isinstance(i, int)])) /
				2
			)
			self._keys.insert(index, average_key)
			self._values.insert(index, value)

	# DONE
	def replace_by_index(self, index: int, value: Any):
		self._values[index] = value

	def replace_key(self, original: Any, replacement: Any):
		"""
			Find and Replace Key
		"""
		self._keys[self._keys.index(original)] = replacement

	def replace_key_slice(self, keys: slice, new_keys: MS):
		self._keys[keys] = new_keys

	# DONE
	def replace_key_by_index(self, index: int, key: Any):
		self._keys[index] = key

	# DONE
	def replace_key_by_value(self, value: Any, key: Any):
		self._keys[self._values.index(value)] = key

	def append(self, iterable: Any):
		self._extend(iterable, keys_list = self._keys, value_list = self._values)

	def extend(self, *args: Tuple[Any], **kwargs: Dict[str, Any]):
		self._extend(args, keys_list = self._keys, value_list = self._values)
		self._extend(kwargs, keys_list = self._keys, value_list = self._values)

	def values(self) -> List[Any]:
		return self._values

	def keys(self) -> List[Any]:
		return self._keys

	def items(
		self,
		whole: bool = False,
		addict: bool = False,
		ordered: bool = False,
		indexed: bool = False,
	):
		if whole:
			if addict:
				dct_type: type = D
			elif ordered:
				dct_type: type = odd
			elif all((addict, ordered)):
				raise TypeError("Sorry! You can't set both 'addict' and 'ordered' at once!")
			else:
				dct_type: type = dict
		else:
			dct_type: type = dict
		dct: Dict[Any, Any] = dct_type(zip(self._keys, self._values))
		if whole:
			return dct
		elif indexed:
			kvnt: type = namedtuple("kvnt", "key value")
			return dct_type({index : kvnt(*kv) for index, kv in enumerate(dct.items())})
		elif all((whole, indexed)):
			raise TypeError("Sorry! You can't set both 'whole' and 'indexed' at once!")
		else:
			return dct.items()

	def pop(self, n: int = 1, front: bool = False):
		popped_keys: List[Any] = []
		popped_values: List[Any] = []

		for i in range(n):
			popped_keys.append(
				self._keys.pop(i if front else len(self._keys) - 1 - i)
			)
			popped_values.append(
				self._values.pop(i if front else len(self._values) - 1 - i)
			)

		return dict(zip(popped_keys, popped_values))

	def get(self, n: int = 1, front: bool = False):
		gotten_keys: List[Any] = []
		gotten_values: List[Any] = []

		for i in range(n):
			gotten_keys.append(
				self._keys[i if front else len(self._keys) - 1 - i]
			)
			gotten_values.append(
				self._values[i if front else len(self._values) - 1 - i]
			)

		return dict(zip(gotten_keys, gotten_values))

	def remove(self, key: Union[str, int]):
		removed: namedtuple = namedtuple("removed", "key value")
		keys_index: int = self._keys.index(key)
		return removed(
			key = self._keys.pop(keys_index),
			value = self._values.pop(keys_index),
		)
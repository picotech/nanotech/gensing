# From Imports
from itertools import chain
from typing import MutableSequence as MS, Any, Dict, List, Union

class _process_extensions:
	def _extend(
		self,
		iterable: MS[Any],
		keys: Union[MS[Any], None] = None,
		keys_list: Union[MS[Any], None] = None,
		values: Union[MS[Any], None] = None,
		value_list: Union[MS[Any], None] = None,
	):
		if keys_list is not None:
			getattr(keys_list, "extend")(
				self._process_keys(iterable, keys = keys or [])
			)
		if value_list is not None:
			getattr(value_list, "extend")(
				self._process_value(iterable, values = values or [])
			)

	def __next_key(self):
		try:
			numeric_keys = [i for i in self._keys if isinstance(i, int)]
			numeric_keys.sort()
			next_key = numeric_keys[-1]
		except IndexError:
			return 0
		else:
			return next_key + 1

	def _process_keys(self, iterable: Any, keys: Union[MS[Any], None] = None):
		keys = keys or []

		if isinstance(iterable, (str, bytes, bytearray)):
			keys.append(self.__next_key())
		elif isinstance(iterable, (dict, self.__class__)):
			keys.extend(list(iterable.keys()))
		else:
			keys.extend(self.__iterables_keys(iterable))

		return keys

	def __iterables_keys(self, iterable: MS[Any]):
		try:
			start_key: int = self.__next_key()
			keys_list = []
			for _iterable in iterable:
				if isinstance(_iterable, (dict, self.__class__)):
					keys_list.extend(_iterable.keys())
				elif self.__is_iterable(_iterable):
					for index in range(len(_iterable)):
						keys_list.append(start_key)
						start_key += 1
				else:
					keys_list.append(start_key)
					start_key += 1
			return keys_list
		except TypeError:
			return [self.__next_key()]

	def _process_value(self, iterable: Any, values: Union[MS[Any], None] = None):
		values = values or []

		if isinstance(iterable, str):
			values.append(iterable)
		elif isinstance(iterable, (bytes, bytearray)):
			values.append(iterable.decode())
		elif isinstance(iterable, (dict, self.__class__)):
			values.extend(list(iterable.values()))
		else:
			values.extend(self.__iterables_values(iterable))

		return values

	def __iterables_values(self, iterable: MS[Any]):
		try:
			value_list = []
			for _iterable in iterable:
				if isinstance(_iterable, (dict, self.__class__)):
					value_list.extend(_iterable.values())
				elif self.__is_iterable(_iterable):
					value_list.extend(_iterable)
				else:
					value_list.append(_iterable)
			return value_list
		except TypeError:
			return [iterable]

	def __is_iterable(self, _iterable: Any):
		try:
			iter(_iterable)
		except TypeError:
			return False
		else:
			return False if isinstance(_iterable, (str, bytes, bytearray)) else True
# From Imports
from typing import Any, Union, List

class _gin:
	def _gin(self, delimiter: str = " ", override: bool = False, o_type: type = str):

        # I don't think this is a shallow copy, as "_values" is a property
		values: MS[Any] = self._values

		if not self._values:
			return o_type()

		if override:
			values = [o_type(_) for _ in values]

		try:
			type_0: Any = type(values[0])
		except IndexError:
			pass
		else:
			if type_0 == str:
				return delimiter.join(str(value) for value in values if value)
			elif type_0 == int:
				return sum(int(value) for value in values if value)
			elif all(isinstance(value, type_0) for value in values):
				total: Any = values[0]
				for value in values[1::]:
					total += value
				return total
			else:
				raise TypeError("Sorry! All values in the tea must be of the same type to join!")

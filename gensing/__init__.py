# From Imports
from autoslot import Slots
from collections import namedtuple
from copy import deepcopy
from nanite import fullpath, mixinport
from typing import MutableSequence as MS, Any, Union, Dict, Tuple, Generator, List

mixins: Generator[str, None, None] = (fullpath(f"{mixin}.py", f_back = 2) for mixin in (
	"_gin",
	"_process_extensions",
	"_short_funcs",
	"glue_",
))

class tea(*(mixinport(mixins))):
	@property
	def _keys(self):
		return self.__keys

	@_keys.setter
	def _keys(self, iterable):
		self.__keys = self._process_keys(iterable, keys = self.__keys)

	@property
	def _values(self):
		return self.__values

	@_values.setter
	def _values(self, iterable):
		self.__values = self._process_value(iterable, values = self.__values)

	def __init__(
		self,
		*args: Tuple[Any],
		keys: MS[Any] = (),
		values: MS[Any] = (),
		**kwargs: Dict[str, Any]
	):

		self.__keys = []
		self.__values = []

		kwargs.update(dict(zip(keys, values)))

		self._keys = self._values = args
		self._extend(kwargs, keys_list = self._keys, value_list = self._values)

	def __call__(
		self,
		delimiter: str = " ",
		override: bool = False,
		o_type: type = str
	):
		return self._gin(delimiter = delimiter, override = override, o_type = o_type)

	def __iter__(self):
		self.n = 0
		return self

	def __next__(self):
		if self.n <= len(self._values) - 1:
			self.n = self.n + 1
			return self._values[self.n - 1]
		else:
			raise StopIteration

	def __len__(self):
		return len(self._values)

	def __add__(self, summand: Any):
		total_keys: List[Any] = self._keys
		total_values: List[Any] = self._values
		self._extend(summand, keys_list = total_keys, value_list = total_values)
		return self.__class__(keys = total_keys, values = total_values)

# 	def __deepcopy__(self):
# 		pass

# 	def __copy__(self):
# 		pass

	def __sub__(self, subtrahend: Any):
		keys = (i for i in self._keys if i not in self._process_keys(subtrahend))
		values = (i for i in self._values if i not in self._process_value(subtrahend))
		return self.__class__(
			keys = keys,
			values = values,
		)

	# def __getattr__(self, attr):
	# 	pass

	"""
		Answer: https://stackoverflow.com/questions/16033017/how-to-override-the-slice-functionality-of-list-in-its-derived-class/16033058#16033058
		User: https://stackoverflow.com/users/100297/martijn-pieters
	"""

	def __getitem__(self, key: Any):
		if isinstance(key, slice):
			keys_or_values = [None] if key.start is None else key.start.split(":")
			if keys_or_values[0] == "keys":
				return self._keys[slice(int(keys_or_values[1]), key.stop, key.step)]
			elif keys_or_values[0] == "values":
				return self._values[slice(int(keys_or_values[1]), key.stop, key.step)]
			else:
				return {
					self._keys[i] : self._values[i] for i in range(
						0 if key.start is None else key.start,
						len(self._keys) if key.stop is None else key.stop,
						1 if key.step is None else key.step
					)
				}

		index: int = self._keys.index(key)
		ikv: type = namedtuple("ikv", "index key value")
		return ikv(index = index, key = key, value = self._values[index])

	def __setitem__(self, key: int, item: Any):
		if isinstance(key, slice):
			keys_or_values = [None] if key.start is None else key.start.split(":")
			if keys_or_values[0] == "keys":
				# DONE
				if isinstance(item, (dict, self.__class__)):
					self._keys[slice(int(keys_or_values[1]), key.stop, key.step)] = list(item.keys())
				else:
					self._keys[slice(int(keys_or_values[1]), key.stop, key.step)] = item
			elif keys_or_values[0] == "values":
				# DONE
				if isinstance(item, (dict, self.__class__)):
					self._values[slice(int(keys_or_values[1]), key.stop, key.step)] = list(item.values())
				else:
					self._values[slice(int(keys_or_values[1]), key.stop, key.step)] = item
			else:
				if isinstance(item, (dict, self.__class__)):
					self._keys[key] = item.keys()
					self._values[key] = item.values()
				else:
					self._values[key] = item
		else:
			try:
				self._values[self._keys.index(key)] = item
			except ValueError:
				self._keys.append(key)
				self._values.append(item)

	def __delitem__(self, key: int):
		if isinstance(key, slice):
			del self._keys[key]
			del self._values[key]
		else:
			index: int = self._keys.index(key)
			del self._values[index]
			del self._keys[index]

	def __bool__(self):
		return all((bool(self._keys), bool(self._values)))

	def __str__(self):
		return self._gin(override = True)

	def __repr__(self):
		return repr(self.items(whole = True, ordered = True))

	def __nonzero__(self):
		return all((bool(self._keys), bool(self._values)))

class frosting(tea, Slots):
	def __init__(self, output, capture = "stdout"):
		self.output = output
		self.string_like_output = isinstance(output, (str, bytes, bytearray))
		self.dict_like_output = isinstance(output, (dict, self.__class__, tea))

		try:
			iter(output)
		except TypeError:
			self.non_iterable_output = True
		else:
			self.non_iterable_output = False

		if self.string_like_output or self.non_iterable_output:
			super().__init__(self.output)
		elif self.dict_like_output:
			super().__init__(**self.output)
		else:
			super().__init__(*self.output)

		self.capture = "stderr" if capture == "stderr" else "stdout"

		for key, value in self.items():
			setattr(self, str(key), value)
	def __iter__(self):
		self.n = 0

		if self.dict_like_output:
			self.next_output = list(getattr(self, self.capture))
		else:
			self.next_output = self.values()

		return self
	def __next__(self):
		if self.n < len(self.next_output):
			self.n += 1
			return self.next_output[self.n - 1]
		else:
			raise StopIteration
	def __str__(self):
		return str(self.output) if self.string_like_output else self._gin(override = True)
	def __repr__(self):
		if self.string_like_output:
			return repr(self.output)
		elif self.dict_like_output:
			return repr(self.items(whole = True, addict = True))
		else:
			return repr(self.values())
	def __call__(self):
		if self.string_like_output or self.non_iterable_output:
			return self.output
		elif self.dict_like_output:
			return self.items(whole = True, addict = True)
		else:
			return self.values()
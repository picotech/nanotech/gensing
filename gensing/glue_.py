# From Imports
from typing import MutableSequence as MS, Any

class glue_:
	"""

		If iterable is a string, combine last value in self._values with iterable, otherwise, combine the last value in self._values with the first items in iterable

	"""
	def glue(self, iterable: MS[Any], override: bool = False, o_type: type = str):
		iterable = self._process_value(iterable)

		"""
			Answer: https://stackoverflow.com/questions/21062781/shortest-way-to-get-first-item-of-ordereddict-in-python-3/27666721#27666721
			User: https://stackoverflow.com/users/27342/ralien
		"""
		seng_last = self._values.pop()
		iterable_first = iterable.pop(0)
		if type(seng_last) == type(iterable_first):
			glued_value: Any = self._process_value(seng_last + iterable_first)
		else:
			if override:
				glued_value: Any = self._process_value(o_type(seng_last) + o_type(iterable_first))
			else:
				raise TypeError("Sorry! The first and last values of the these two teas' must be of the same type!")

		self._values.extend(glued_value)
		self._extend(iterable, keys_list = self._keys, value_list = self._values)